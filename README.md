# Cooler Controlado pela Temperatura

Projeto de um sistema que muda a intensidade de rotação de um cooler para diminmuir a temperatura, informada por um sensor, de um aparelho,  com led rgb e um alarme para indicar, visualmente e sonoramente, a quantidade de temperatura.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
| Mateus Ruaro Busato | @mateusbusato|
| Rafael Boldrini Demezuk| @RafaelDemezuk|
| Marcio Greco Junior| @marciogrco|
# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://mateusbusato.gitlab.io/ie21cp20202/

# Links Úteis

* [Projeto no Tinkercad](https://www.tinkercad.com/things/bS6bOzEpFhp-sizzling-kup-wluff/editel?sharecode=lkwz8gO4wu3j_rLSipKAC7EzenGNjvWyc6ijCotWXvc)

