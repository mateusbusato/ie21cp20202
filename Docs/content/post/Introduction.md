---
title: ""
date: 2021-05-06T16:51:04-03:00
draft: false
---
# Introdução ao Tema

Esse projeto é sobre um cooler controlado pela temperatura.

# Objetivos

* Desenvolver um cooler que mude a intensidade de rotação com base na mudança de temperatura.
* Fazer um sistema visual ultilizando um led rgb para informar a temperatura de forma simple e pratica.
* Fazer um sistema sonoro ultilizando um piezo para informar quando o cooler está no máximo de rotações por minuto.

# Materiais Utilizados
* Arduino Uno R3
* Protoboard pequena
* LED RGB
* 4 Resistores (1 de 1KΩ e 3 de 220Ω)
* Sensor de temperatura TMP36
* Piezo
* Cooler (Motor CC)

# Diagrama Elétrico

![](Arduino.png)

# Código
```Cpp
#define TMP A0
#define MOT 3
#define RED 5
#define GREEN 6
#define BLUE 9
#define BUZZ 10

void setup()
{
	pinMode(MOT,OUTPUT);
	pinMode(RED, OUTPUT);
	pinMode(GREEN, OUTPUT);
	pinMode(BLUE, OUTPUT);
	pinMode(BUZZ, OUTPUT);
}

void loop()
{
	
	int sensor = analogRead(TMP); 
	float tensao = sensor * (5000 / 1024.0);
 	float temp = (tensao - 500) / 10;
 	
 	
   	if(temp< 50.0)
    {
		analogWrite(MOT,76);
      	analogWrite(RED,0);
    	analogWrite(GREEN,255);
    	analogWrite(BLUE,0);
    }
 	else if(temp >= 50.0 && temp < 70.0)
 	{
 		analogWrite(MOT,102);
 		analogWrite(RED,255);
    	analogWrite(GREEN,60);
    	analogWrite(BLUE,0);
    	digitalWrite(BUZZ,0);
	}
	else if(temp >= 70.0)
	{
		analogWrite(MOT,255);
		analogWrite(RED,255);
    	analogWrite(GREEN,0);
    	analogWrite(BLUE,0);
    	digitalWrite(BUZZ,1);
	}
	
}
``` 

# Resultados 
Vídeo de demonstração:

{{< youtube w_yqDLsjaFE >}}

# Desafios encontrados

Pensar em uma aplicação para o projeto feito, e uma das maiores dificuldades foi compreender como fazer o site.
